# IA Wavetronix Incident Detection

This project collects wavetronix smoothing data (in csv format) every 20 seconds and runs incident detection algorithm on the data and outputs the incidents in the DynamoDB table.

## Getting Started

### Prerequisites
To succesfully run this project, you need JAVA 8, Maven and Spark.
Installing java on ubuntu: https://www.digitalocean.com/community/tutorials/how-to-install-java-with-apt-get-on-debian-8

Installing maven on ubuntu: https://www.vultr.com/docs/how-to-install-apache-maven-on-ubuntu-16-04

Installing Spark on ubuntu: 

1. Install spark: wget http://www-us.apache.org/dist/spark/spark-2.3.3/spark-2.3.3-bin-hadoop2.7.tgz 
2. tar xvzf spark-2.3.2-bin-hadoop2.7.tgz
3. SPARK_HOME=/DeZyre/spark 
4. export PATH=$SPARK_HOME/bin:$PATH
5. source  ~/.bashrc
6. ./bin/spark-shell

### Installing

To run this project on your local machine:

1. git clone https://ravitejabynaetalyc@bitbucket.org/ravitejabynaetalyc/ia-wavetronix-incident-detection.git
2. cd ia-wavetronix-incident-detection
3. Go to applicaton.properties file in the project's root folder and edit "archive.wavetronix.realtimedata.folder=<local_data_store_path>" - the path where you want to retrieve all the smoothing data. Example: "archive.wavetronix.realtimedata.folder=/Users/ravitejarajabyna/etalyc/data"
3. Run "mvn clean install -DskipTests"
4. Open the project in IDE and Run/Debug ReadingStreams.java for running Spark App.
5. OR Run "nohup spark-2.3.3-bin-hadoop2.7/bin/spark-submit incident-detection.jar -Xms256m -Xmx1024m &" for running Spark App. 
6. To confirm if the incident algorithn is running, check the nohup.out (tail -1000f nohup.out). Note: Check if the smoothing data is getting generated or not since it takes 40 mintues at the start of the process to output the file.

## Deployment

Currently, this data pipeline is running on aws ubuntu machines.

 - ssh -i "etalyc_camera.pem" ubuntu@ec2-34-229-228-181.compute-1.amazonaws.com

To deploy:

1. Run the project as mentioned above locally and generate the jar files.
2. scp -i "/Users/ravitejarajabyna/Downloads/etalyc_camera.pem" target/ia-wavetronix-incident-detection-1.0-SNAPSHOT.jar ubuntu@ec2-34-229-228-181.compute-1.amazonaws.com:incident-detection.jar
3. ssh into the machine (ssh -i "etalyc_camera.pem" ubuntu@ec2-34-229-228-181.compute-1.amazonaws.com)
4. Kill the running jobs for "incident-detection.jar"
5. Then run "nohup spark-2.3.3-bin-hadoop2.7/bin/spark-submit incident-detection.jar -Xms256m -Xmx1024m &".

## Debugging
1. Check nohup.out for logs

## Functionality

	Input: /home/ubuntu/data/iadot/smoothingdata/realtimedata/20190429.csv  (s3 bucket)
	Output: iawavetronixincident (DynamoDB Table)
	
1. Convert the input wavetronix stream to object
2. Filter non-operational sensors
3. The main logic starts here, where processed data is being used and incidents are detected. Threshold data is stored in RedisDB which gets updated every month manually. Incident is detected as POTENTIAL INCIDENT if the speed is less than the threshold speed for a given segment.
	I. If the above condition occurs consecutively for 3 times, we consider it as an incident and
	store it in database and a unique event_id is assigned to it.

	II. If the above condition occurs consecutively for 3 times and adjacent codes are already an
	incident then same event_id is assigned to it and consider to be one incident. 23

	III. Also, If the above condition occurs consecutively for 3 times and there is an incident
	occured within last 60 minutes then same event_id is assigned and stored in database.
	
4. Incident is detected as MAY_BE_OVER if the segment is already considered to have an incident
	in previous minute and the speed is greater than threshold value.

	I. If the above condition occurs consecutively for 5 minutes then that segment is no longer
	an incident and database is updated with end_timestamp.
	
5. If it is an INCIDENT then check if it is a duplicate or not since Spark paralelly computes each and every segment and when the incident occurs at the same time for two segments then same event_id needs to be assigned.
6. At last, store the incident in Dynamo DB.

## Known Issues
1. Sometimes, the spark fails to get the smoothing data, even though the smoothing data is successfully getting generated.
2. Sometimes, duplicate event_ids are generated.

## Authors

* **Raviteja Raja Byna** - *Initial work* - Github (https://github.com/BRaviteja)

## License





