package com.etalyc.mcomp.incidentdetection.obj;

import java.io.Serializable;

public class LocationData implements Serializable {

    private String detector_Id;
    private String organization_Id;
    private String network_Id;
    private String local_date;
    private String local_time;
    private String utc_offset;
    private String station_Id;
    private String detector_name;
    private String latitude;
    private String longitude;
    private String link_ownership;
    private String route_designator;
    private String linear_reference;
    private String detector_type;
    private String approach_direction;
    private String approach_name;
    private String lanes_type;
    private String lane_id;
    private String lane_name;

    public LocationData(String detector_Id, String organization_Id, String network_Id, String local_date, String local_time,
                        String utc_offset, String station_Id, String detector_name, String latitude, String longitude,
                        String link_ownership, String route_designator, String linear_reference, String detector_type,
                        String approach_direction, String approach_name, String lanes_type, String lane_id, String lane_name) {
        this.detector_Id = detector_Id;
        this.organization_Id = organization_Id;
        this.network_Id = network_Id;
        this.local_date = local_date;
        this.local_time = local_time;
        this.utc_offset = utc_offset;
        this.station_Id = station_Id;
        this.detector_name = detector_name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.link_ownership = link_ownership;
        this.route_designator = route_designator;
        this.linear_reference = linear_reference;
        this.detector_type = detector_type;
        this.approach_direction = approach_direction;
        this.approach_name = approach_name;
        this.lanes_type = lanes_type;
        this.lane_id = lane_id;
        this.lane_name = lane_name;
    }

    public LocationData() {
    }

    public String getOrganization_Id() {
        return organization_Id;
    }

    public void setOrganization_Id(String organization_Id) {
        this.organization_Id = organization_Id;
    }

    public String getNetwork_Id() {
        return network_Id;
    }

    public void setNetwork_Id(String network_Id) {
        this.network_Id = network_Id;
    }

    public String getLocal_date() {
        return local_date;
    }

    public void setLocal_date(String local_date) {
        this.local_date = local_date;
    }

    public String getLocal_time() {
        return local_time;
    }

    public void setLocal_time(String local_time) {
        this.local_time = local_time;
    }

    public String getUtc_offset() {
        return utc_offset;
    }

    public void setUtc_offset(String utc_offset) {
        this.utc_offset = utc_offset;
    }

    public String getDetector_Id() {
        return detector_Id;
    }

    public void setDetector_Id(String detector_Id) {
        this.detector_Id = detector_Id;
    }

    public String getStation_Id() {
        return station_Id;
    }

    public void setStation_Id(String station_Id) {
        this.station_Id = station_Id;
    }

    public String getDetector_name() {
        return detector_name;
    }

    public void setDetector_name(String detector_name) {
        this.detector_name = detector_name;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLink_ownership() {
        return link_ownership;
    }

    public void setLink_ownership(String link_ownership) {
        this.link_ownership = link_ownership;
    }

    public String getRoute_designator() {
        return route_designator;
    }

    public void setRoute_designator(String route_designator) {
        this.route_designator = route_designator;
    }

    public String getLinear_reference() {
        return linear_reference;
    }

    public void setLinear_reference(String linear_reference) {
        this.linear_reference = linear_reference;
    }

    public String getDetector_type() {
        return detector_type;
    }

    public void setDetector_type(String detector_type) {
        this.detector_type = detector_type;
    }

    public String getApproach_direction() {
        return approach_direction;
    }

    public void setApproach_direction(String approach_direction) {
        this.approach_direction = approach_direction;
    }

    public String getApproach_name() {
        return approach_name;
    }

    public void setApproach_name(String approach_name) {
        this.approach_name = approach_name;
    }

    public String getLanes_type() {
        return lanes_type;
    }

    public void setLanes_type(String lanes_type) {
        this.lanes_type = lanes_type;
    }

    public String getLane_id() {
        return lane_id;
    }

    public void setLane_id(String lane_id) {
        this.lane_id = lane_id;
    }

    public String getLane_name() {
        return lane_name;
    }

    public void setLane_name(String lane_name) {
        this.lane_name = lane_name;
    }

    @Override
    public String toString() {
        return "LocationData{" +
                "detector_Id='" + detector_Id + '\'' +
                ", organization_Id='" + organization_Id + '\'' +
                ", network_Id='" + network_Id + '\'' +
                ", local_date='" + local_date + '\'' +
                ", local_time='" + local_time + '\'' +
                ", utc_offset='" + utc_offset + '\'' +
                ", station_Id='" + station_Id + '\'' +
                ", detector_name='" + detector_name + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", link_ownership='" + link_ownership + '\'' +
                ", route_designator='" + route_designator + '\'' +
                ", linear_reference='" + linear_reference + '\'' +
                ", detector_type='" + detector_type + '\'' +
                ", approach_direction='" + approach_direction + '\'' +
                ", approach_name='" + approach_name + '\'' +
                ", lanes_type='" + lanes_type + '\'' +
                ", lane_id='" + lane_id + '\'' +
                ", lane_name='" + lane_name + '\'' +
                '}';
    }
}
