package com.etalyc.mcomp.incidentdetection.obj;

import java.io.Serializable;
import java.util.Date;

public class DetectorLocationData implements Serializable {
	public DetectorLocationData() {

	}

	private String event_id;
	private String start_timestamp;
	private String end_timestamp;
	private String threshold_speed;
	private double speed;
	private double avg_speed;
	private String author;
	private boolean incident_inprogress;
	private int incident_count;
	private int currentIncident;
	private String detector_Id;
	private String organization_Id;
	private String network_Id;
	private String local_date;
	private String local_time;
	private String utc_offset;
	private String station_Id;
	private String detector_name;
	private String start_lat;
	private String start_long;
	private String link_ownership;
	private String roadname;
	private String linear_reference;
	private String detector_type;
	private String bearing;
	private String approach_name;
	//	@DateTimeFormat(iso= DateTimeFormat.ISO.DATE_TIME)
	private Date start_timestamp_date_format;

	public Date getStart_timestamp_date_format() {
		return start_timestamp_date_format;
	}

	public void setStart_timestamp_date_format(Date start_timestamp_date_format) {
		this.start_timestamp_date_format = start_timestamp_date_format;
	}

	public Date getEnd_timestamp_date_format() {
		return end_timestamp_date_format;
	}

	public void setEnd_timestamp_date_format(Date end_timestamp_date_format) {
		this.end_timestamp_date_format = end_timestamp_date_format;
	}

	//	@DateTimeFormat(iso= DateTimeFormat.ISO.DATE_TIME)
	private Date end_timestamp_date_format;

	public boolean isDuplicate() {
		return duplicate;
	}

	public void setDuplicate(boolean duplicate) {
		this.duplicate = duplicate;
	}

	private boolean duplicate;

	public DetectorLocationData(String event_id, String start_timestamp, String end_timestamp,
								String threshold_speed, double speed, double avg_speed, String author,
								boolean incident_inprogress, int incident_count, int currentIncident,
								String detector_Id, String organization_Id, String network_Id, String local_date,
								String local_time, String utc_offset, String station_Id, String detector_name,
								String start_lat, String start_long, String link_ownership, String roadname,
								String linear_reference, String detector_type, String bearing,
								String approach_name, Date start_timestamp_date_format, Date end_timestamp_date_format, boolean duplicate) {
		this.event_id = event_id;
		this.start_timestamp = start_timestamp;
		this.end_timestamp = end_timestamp;
		this.threshold_speed = threshold_speed;
		this.speed = speed;
		this.avg_speed = avg_speed;
		this.author = author;
		this.incident_inprogress = incident_inprogress;
		this.incident_count = incident_count;
		this.currentIncident = currentIncident;
		this.detector_Id = detector_Id;
		this.organization_Id = organization_Id;
		this.network_Id = network_Id;
		this.local_date = local_date;
		this.local_time = local_time;
		this.utc_offset = utc_offset;
		this.station_Id = station_Id;
		this.detector_name = detector_name;
		this.start_lat = start_lat;
		this.start_long = start_long;
		this.link_ownership = link_ownership;
		this.roadname = roadname;
		this.linear_reference = linear_reference;
		this.detector_type = detector_type;
		this.bearing = bearing;
		this.approach_name = approach_name;
		this.startTimestampDate = new Date();
		this.start_timestamp_date_format = start_timestamp_date_format;
		this.end_timestamp_date_format = end_timestamp_date_format;
		this.duplicate = duplicate;
	}

	public Date getStartTimestampDate() {
		return startTimestampDate;
	}

	public void setStartTimestampDate(Date startTimestampDate) {
		this.startTimestampDate = startTimestampDate;
	}

	private Date startTimestampDate;

	public String getEvent_id() {
		return event_id;
	}

	public void setEvent_id(String event_id) {
		this.event_id = event_id;
	}

	public String getStart_timestamp() {
		return start_timestamp;
	}

	public void setStart_timestamp(String start_timestamp) {
		this.start_timestamp = start_timestamp;
	}

	public String getEnd_timestamp() {
		return end_timestamp;
	}

	public void setEnd_timestamp(String end_timestamp) {
		this.end_timestamp = end_timestamp;
	}

	public String getThreshold_speed() {
		return threshold_speed;
	}

	public void setThreshold_speed(String threshold_speed) {
		this.threshold_speed = threshold_speed;
	}

	public double getSpeed() {
		return speed;
	}

	public void setSpeed(double speed) {
		this.speed = speed;
	}

	public double getAvg_speed() {
		return avg_speed;
	}

	public void setAvg_speed(double avg_speed) {
		this.avg_speed = avg_speed;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public boolean isIncident_inprogress() {
		return incident_inprogress;
	}

	public void setIncident_inprogress(boolean incident_inprogress) {
		this.incident_inprogress = incident_inprogress;
	}

	public int getIncident_count() {
		return incident_count;
	}

	public void setIncident_count(int incident_count) {
		this.incident_count = incident_count;
	}

	public int getCurrentIncident() {
		return currentIncident;
	}

	public void setCurrentIncident(int currentIncident) {
		this.currentIncident = currentIncident;
	}

	public String getDetector_Id() {
		return detector_Id;
	}

	public void setDetector_Id(String detector_Id) {
		this.detector_Id = detector_Id;
	}

	public String getOrganization_Id() {
		return organization_Id;
	}

	public void setOrganization_Id(String organization_Id) {
		this.organization_Id = organization_Id;
	}

	public String getNetwork_Id() {
		return network_Id;
	}

	public void setNetwork_Id(String network_Id) {
		this.network_Id = network_Id;
	}

	public String getLocal_date() {
		return local_date;
	}

	public void setLocal_date(String local_date) {
		this.local_date = local_date;
	}

	public String getLocal_time() {
		return local_time;
	}

	public void setLocal_time(String local_time) {
		this.local_time = local_time;
	}

	public String getUtc_offset() {
		return utc_offset;
	}

	public void setUtc_offset(String utc_offset) {
		this.utc_offset = utc_offset;
	}

	public String getStation_Id() {
		return station_Id;
	}

	public void setStation_Id(String station_Id) {
		this.station_Id = station_Id;
	}

	public String getDetector_name() {
		return detector_name;
	}

	public void setDetector_name(String detector_name) {
		this.detector_name = detector_name;
	}

	public String getStart_lat() {
		return start_lat;
	}

	public void setStart_lat(String start_lat) {
		this.start_lat = start_lat;
	}

	public String getStart_long() {
		return start_long;
	}

	public void setStart_long(String start_long) {
		this.start_long = start_long;
	}

	public String getLink_ownership() {
		return link_ownership;
	}

	public void setLink_ownership(String link_ownership) {
		this.link_ownership = link_ownership;
	}

	public String getRoadname() {
		return roadname;
	}

	public void setRoadname(String roadname) {
		this.roadname = roadname;
	}

	public String getLinear_reference() {
		return linear_reference;
	}

	public void setLinear_reference(String linear_reference) {
		this.linear_reference = linear_reference;
	}

	public String getDetector_type() {
		return detector_type;
	}

	public void setDetector_type(String detector_type) {
		this.detector_type = detector_type;
	}

	public String getBearing() {
		return bearing;
	}

	public void setBearing(String bearing) {
		this.bearing = bearing;
	}

	public String getApproach_name() {
		return approach_name;
	}

	public void setApproach_name(String approach_name) {
		this.approach_name = approach_name;
	}

}
