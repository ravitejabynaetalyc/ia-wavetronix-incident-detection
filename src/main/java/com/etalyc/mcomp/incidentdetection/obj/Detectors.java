package com.etalyc.mcomp.incidentdetection.obj;

import java.io.Serializable;

//detector-ID, local-date, start-time, end-time, timeStartSec, timeEndSec, volume, occu/numberOfLanes, speed/vol*0.621371, smoothedOcc, smoothedSpeed, predictedMsg, status
public class Detectors implements Serializable {

    public Detectors() {

    }
    private String detectorId;
    private String localDate;
    private String starttime;
    private String endtime;
    private String timeStartSec;
    private String timeEndSec;
    private String volume;
    private String avgOccupancy;
    private String avgSpeed;
    private String status;
    private String smoothedOcc;
    private String smoothedSpeed;
    private String predictedMsg;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Detectors(String detectorId, String localDate, String starttime, String endtime, String timeStartSec, String timeEndSec, String volume, String avgOccupancy, String avgSpeed, String status, String smoothedOcc, String smoothedSpeed, String predictedMsg) {
        this.detectorId = detectorId;
        this.localDate = localDate;
        this.starttime = starttime;
        this.endtime = endtime;
        this.timeStartSec = timeStartSec;
        this.timeEndSec = timeEndSec;
        this.volume = volume;
        this.avgOccupancy = avgOccupancy;
        this.avgSpeed = avgSpeed;
        this.smoothedOcc = smoothedOcc;
        this.smoothedSpeed = smoothedSpeed;
        this.predictedMsg = predictedMsg;
        this.status = status;
    }


    public String getDetectorId() {
        return detectorId;
    }

    public void setDetectorId(String detectorId) {
        this.detectorId = detectorId;
    }

    public String getLocalDate() {
        return localDate;
    }

    public void setLocalDate(String localDate) {
        this.localDate = localDate;
    }

    public String getStarttime() {
        return starttime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    public String getEndtime() {
        return endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    public String getTimeStartSec() {
        return timeStartSec;
    }

    public void setTimeStartSec(String timeStartSec) {
        this.timeStartSec = timeStartSec;
    }

    public String getTimeEndSec() {
        return timeEndSec;
    }

    public void setTimeEndSec(String timeEndSec) {
        this.timeEndSec = timeEndSec;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getAvgOccupancy() {
        return avgOccupancy;
    }

    public void setAvgOccupancy(String avgOccupancy) {
        this.avgOccupancy = avgOccupancy;
    }

    public String getAvgSpeed() {
        return avgSpeed;
    }

    public void setAvgSpeed(String avgSpeed) {
        this.avgSpeed = avgSpeed;
    }

    public String getSmoothedOcc() {
        return smoothedOcc;
    }

    public void setSmoothedOcc(String smoothedOcc) {
        this.smoothedOcc = smoothedOcc;
    }

    public String getSmoothedSpeed() {
        return smoothedSpeed;
    }

    public void setSmoothedSpeed(String smoothedSpeed) {
        this.smoothedSpeed = smoothedSpeed;
    }

    public String getPredictedMsg() {
        return predictedMsg;
    }

    public void setPredictedMsg(String predictedMsg) {
        this.predictedMsg = predictedMsg;
    }
}
