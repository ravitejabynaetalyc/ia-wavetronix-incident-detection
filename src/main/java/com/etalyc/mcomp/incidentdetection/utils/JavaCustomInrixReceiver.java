package com.etalyc.mcomp.incidentdetection.utils;


import org.apache.commons.io.FileUtils;
import org.apache.spark.storage.StorageLevel;
import org.apache.spark.streaming.receiver.Receiver;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.ConnectException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileAttribute;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.function.Consumer;
import java.util.stream.Stream;

public class JavaCustomInrixReceiver extends Receiver<String>{
    public static final String PATTERN_MM_DD_YYYY = "MM-dd-yyyy";

    public static String _dir;

    private static String file;

    public JavaCustomInrixReceiver(String _dir, String file){
    	

        super(StorageLevel.MEMORY_AND_DISK_2());
        this._dir = _dir;
        this.file =file;
        
    }

    @Override
    public void onStart(){
        new Thread(this::receive).start();
    }

    @Override
    public void onStop() {
        // There is nothing much to do as the thread calling receive()
        // is designed to stop by itself if isStopped() returns false
    }

    /** Create a FileStreamResder and keep on receiving data*/
    private void receive() {
        String today = new SimpleDateFormat("yyyyMMdd").format(new Date());
        file = today + ".csv";
        String fileName = null;
        SimpleDateFormat sdf = new SimpleDateFormat(PATTERN_MM_DD_YYYY);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));

        fileName = _dir + "/" + file;
        System.out.println("Filename to be retrieved : " + fileName);


        try {

        	File infile = null;
        	//It is an workaround, should be removed later when file update will be fixed without deletion
        	while(infile == null) {
        		try {
        			infile = new File(fileName);
        		}catch(Exception e) {
        			System.out.println("No such file exists");
        		}
        	}

         	Stream<String> stream = Files.readAllLines(Paths.get(fileName)).stream();

            try {

                stream.forEach(new Consumer<String>() {
                    @Override
                    public void accept(String inrixstring) {
                        store(inrixstring);
                    }
                });
            }finally {

                System.out.println("Start time 1 : " + new Date(System.currentTimeMillis()));
                stream.close();
            }

        	while(true) {
                String today1 = new SimpleDateFormat("yyyyMMdd").format(new Date());
                file = today1 + ".csv";

                fileName = _dir + "/" + file;

//                System.out.println("Filename to be retrieved : " + fileName);

        		File tempFile = null;
        		//It is an workaround, should be removed later when file update will be fixed without deletion
        		while(tempFile == null) {
        			try {
        				tempFile = new File(fileName);
        			} catch(Exception e) {
        				System.out.println("No such file exists");
        			}
        		}
                boolean isTwoEqual = FileUtils.contentEquals(infile, tempFile);
        		if(isTwoEqual)
        		    continue;

                if (tempFile.length() == 0)
                    continue;

                Thread.sleep(3000);

        		infile = tempFile;

        		Stream<String> stream2 = null;


                while(stream2 == null) {
                    try {
                        stream2 = Files.readAllLines(Paths.get(fileName)).stream();
                    } catch(Exception e) {
                        e.printStackTrace();
                    }
                }
                try {

                    stream2.forEach(new Consumer<String>() {
                        @Override
                        public void accept(String inrixDataString) {
                            store(inrixDataString);
                        }
                    });
                } finally {

                    System.out.print("Start time 2 : " + new Date(System.currentTimeMillis()));
                    stream2.close();
                }

        	}

      } catch(ConnectException ce) {
            // restart if could not connect to server
            restart("Could not connect", ce);
        } catch(Throwable t) {
            // restart if there is any other error
            restart("Error receiving data", t);
        }
    }
    private static List<String> diffFiles(final List<String> firstFileContent,
                                          final List<String> secondFileContent) {
        final List<String> diff = new ArrayList<String>();
        for (final String line : firstFileContent) {
            if (!secondFileContent.contains(line)) {
                diff.add(line);
            }
        }
        return diff;
    }

    private boolean isCompletelyWritten(File file) {
        RandomAccessFile stream = null;
        try {
            stream = new RandomAccessFile(file, "rw");
            return true;
        } catch (Exception e) {
            System.out.println("Skipping file " + file.getName() + " for this iteration due it's not completely written");
        } finally {
            if (stream != null) {
                try {
                    stream.close();
                } catch (IOException e) {
                    System.out.println("Exception during closing file " + file.getName());
                }
            }
        }
        return false;
    }
}