package com.etalyc.mcomp.incidentdetection.lib;


import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.etalyc.mcomp.incidentdetection.obj.DetectorLocationData;
import com.etalyc.mcomp.incidentdetection.obj.LocationCodeFreeway;
import com.etalyc.mcomp.incidentdetection.obj.RawInrixData;
import com.etalyc.mcomp.incidentdetection.utils.DateUtils;
import org.bson.Document;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;

import static java.util.stream.Collectors.toList;

public class CsvToJsonConvertor {



    public static String csvToJson() throws IOException {
        JSONArray objects = new JSONArray(Files.readAllLines(Paths.get("src/main/resources/location_codes_freeway.csv"))
                .stream()
                .map(s -> new LocationCodeFreeway(s.split(",")[0], s.split(",")[1], s.split(",")[2], s.split(",")[3],
                        s.split(",")[4], s.split(",")[5], s.split(",")[6], s.split(",")[7]))
                .collect(toList()));
        return objects.toString();

    }

    public static String segmentLocationDataCsvToJson(String s) throws IOException {
//        String[] locationDataArray = s.split(",");
        String[] locationDataArray = s.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", -1);
        String start_timestampString = DateUtils.convertTimestampToStr(locationDataArray[1]);
        Date start_timestamp = DateUtils.convertTime(start_timestampString);
        DetectorLocationData locationData = new DetectorLocationData(locationDataArray[0],locationDataArray[1],
                locationDataArray[2],locationDataArray[3],Double.parseDouble(locationDataArray[4]),Double.parseDouble(locationDataArray[5]),locationDataArray[6],
                Boolean.parseBoolean(locationDataArray[7]), Integer.parseInt(locationDataArray[8]), Integer.parseInt(locationDataArray[9]), locationDataArray[10],
                locationDataArray[11], locationDataArray[12], locationDataArray[13], locationDataArray[14], locationDataArray[15],
                locationDataArray[16], locationDataArray[17], locationDataArray[18], locationDataArray[19],
                locationDataArray[20], locationDataArray[21], locationDataArray[22], locationDataArray[23],
                locationDataArray[24],locationDataArray[25], start_timestamp, null, Boolean.valueOf(locationDataArray[26]));
        JSONObject jsonObject = new JSONObject(locationData);
        return jsonObject.toString();

    }

    public static String rawDataCsvToJson(String s) throws IOException {
//        String[] locationDataArray = s.split(",");
        String[] locationDataArray = s.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", -1);
        RawInrixData locationData = new RawInrixData(locationDataArray[0],
                Double.parseDouble(locationDataArray[1]), Double.parseDouble(locationDataArray[2]),Double.parseDouble(locationDataArray[3]),locationDataArray[4], Integer.parseInt(locationDataArray[5]),
                Integer.parseInt(locationDataArray[6]), locationDataArray[7], locationDataArray[8], Integer.parseInt(locationDataArray[9]),
                locationDataArray[10], locationDataArray[11], locationDataArray[12], locationDataArray[13],
                locationDataArray[14], locationDataArray[15], locationDataArray[16], Double.parseDouble(locationDataArray[17]),
                locationDataArray[18],locationDataArray[19],locationDataArray[20],Double.parseDouble(locationDataArray[21]),
                Double.parseDouble(locationDataArray[22]),Double.parseDouble(locationDataArray[23]),Double.parseDouble(locationDataArray[24]) ,locationDataArray[25], locationDataArray[26], locationDataArray[27], new Date());

        JSONObject jsonObject = new JSONObject(locationData);
        //System.out.println(locationDataArray[0]);
        //System.out.println(jsonObject.toString());
        return jsonObject.toString();

    }

    public static void main(String[] args) throws Exception {
//        String json = segmentLocationDataCsvToJson("1485615236,66,67,30,1485615236,92,XDS,0.522,71,21991,10147850," +
//                "1485615236,1485615253,1485615221,0,35,I 35, ,United States of America,Iowa,Decatur, ,0.6166798395004,0, " +
//                ",I 35,40.83186,-93.81306,40.83938,-93.80670,N,3082440,4326");
//        System.out.println(json);

        String s = segmentLocationDataCsvToJson("22-02-2019-1,Fri Feb 22 14:25:40 UTC 2019, null, 0.0,30.764861519221366,36.039518,Wavetronix,false,3,1,IWZ2998 - I-35 SB @ MM 4.2,IADOT-SIMS,IADOT-SIMS,20181127,175907,-0600,IWZ2998 - I-35 SB @ MM 4.2,IWZ2998 - I-35 SB @ MM 4.2,40.624322,-93.889904,IADOT-AM, ,4.21,Microwave Radar,N,IWZ2999-SB LL,lane,1,first,false");
        System.out.println(s);
        Document doc = Document.parse(s);
        AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard()
//                .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration("http://localhost:8000", "us-west-2"))
                .withRegion(Regions.US_EAST_1)
                .build();

        DynamoDB dynamoDB = new DynamoDB(client);
        Table table = dynamoDB.getTable("iawavetronixincidents");
        DynamoDbUtils.saveIncident(table, doc);
    }


}
