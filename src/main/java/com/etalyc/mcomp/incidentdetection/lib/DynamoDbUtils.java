package com.etalyc.mcomp.incidentdetection.lib;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.*;
import com.amazonaws.services.dynamodbv2.document.spec.ScanSpec;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.ReturnValue;
import com.etalyc.mcomp.incidentdetection.utils.DateUtils;
import org.bson.Document;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class DynamoDbUtils {


    //Document{{utc_offset=-0500, start_long=-91.638001, start_timestamp=20190401_214320, station_Id=IWZ3088 - Penn St EB @ I-380, detector_name=IWZ3088 - Penn St EB @ I-380, incident_inprogress=false, speed=34.46667265624996, approach_name=IWZ3088-EB, network_Id=IADOT-SIMS, local_time=144923, end_timestamp=null, organization_Id=IADOT-SIMS, start_lat=41.752438999999995, startTimestampDate=Mon Apr 01 21:46:20 CDT 2019, detector_Id=IWZ3088 - Penn St EB @ I-380, avg_speed=41.631857000000004, author=Wavetronix, bearing=e, duplicate=false, currentIncident=1, incident_count=3, local_date=20170911, event_id=01-04-2019_1554173180280_IWZ3088 - Penn St EB @ I-380, threshold_speed=0.0, linear_reference=, link_ownership=IADOT-AM, detector_type=Microwave Radar, roadname=Penn St}}
    public static void saveIncident(Table table, Document doc) throws Exception {
        final Map<String, Object> infoMap = new HashMap<String, Object>();
        infoMap.put("plot", "Nothing happens at all.");
        infoMap.put("rating", 0);
        String json = "{\"fid\":1874,\"country\":\"United States of America\",\"start_long\":-93.34952,\"code\":\"1450489749\",\"end_lat\":43.37079,\"shapesrid\":\"4326\",\"slipRoad\":\"0\",\"county\":\"Worth\",\"speed\":67,\"miles\":0.58874920442,\"roadnumber\":\"35\",\"specialroad\":\" \",\"frc\":0,\"start_lat\":43.37931,\"state\":\"Iowa\",\"xdGroup\":\"2624584\",\"linearid\":\" \",\"previous_code\":\"1450489735\",\"avg_speed\":66,\"author\":\"Inrix\",\"bearing\":\"S\",\"roadlist\":\"I 35;IA 27\",\"oid_1\":9813472,\"threshold_speed\":0,\"end_long\":-93.3494,\"district\":\" \",\"next_code\":\"1450489761\",\"roadname\":\"I 35;IA 27\"}\n";
        try {
            System.out.println("Adding a new item...");

            PutItemOutcome outcome = table
                    .putItem(new Item()
                            .withPrimaryKey("event_id", doc.getString("event_id"))
                            .withString("start_timestamp", doc.getString("start_timestamp"))
                            .with("end_timestamp", doc.get("end_timestamp"))
                            .withString("threshold_speed", doc.getString("threshold_speed"))
                            .withNumber("speed", doc.getDouble("speed"))
                            .withNumber("avg_speed", doc.getDouble("avg_speed"))
                            .withString("author", doc.getString("author"))
                            .withBoolean("incident_inprogress", Boolean.valueOf(String.valueOf(doc.get("incident_inprogress"))))
                            .withNumber("incident_count", doc.getInteger("incident_count"))
                            .withNumber("currentIncident", doc.getInteger("currentIncident"))
                            .withString("detector_Id", doc.getString("detector_Id"))
                            .withString("organization_Id", doc.getString("organization_Id"))
                            .withString("network_Id", doc.getString("network_Id"))
                            .withString("local_date", doc.getString("local_date"))
                            .withString("local_time", doc.getString("local_time"))
                            .withString("utc_offset", doc.getString("utc_offset"))
                            .withString("station_Id", doc.getString("station_Id"))
                            .withString("detector_name", doc.getString("detector_name"))
                            .withString("start_lat", doc.getString("start_lat"))
                            .withString("start_long", doc.getString("start_long"))
                            .withString("link_ownership", doc.getString("link_ownership"))
                            .withString("roadname", doc.getString("roadname"))
                            .withString("linear_reference", doc.getString("linear_reference"))
                            .withString("detector_type", doc.getString("detector_type"))
                            .withString("bearing", doc.getString("bearing"))
                            .withString("approach_name", doc.getString("approach_name"))
                            .withNumber("start_timestamp_date_format", System.currentTimeMillis())
                            .withBoolean("duplicate", doc.getBoolean("duplicate")));
//                            .withNumber("end_timestamp_date_format", doc.getDate("end_timestamp_date_format").getTime()));

            System.out.println("PutItem succeeded:\n" + outcome.getPutItemResult());

        }
        catch (Exception e) {
            System.err.println("Unable to add item: " + doc);
            System.err.println(e.getMessage());
        }
    }



    public static boolean updateItem(Table table, String event_id, String detector_id) {

        UpdateItemSpec updateItemSpec = new UpdateItemSpec().withPrimaryKey("event_id", event_id, "detector_Id", detector_id)
                .withUpdateExpression("set end_timestamp = :e, currentIncident= :c, incident_inprogress= :i, end_timestamp_date_format= :ed")
                .withConditionExpression("incident_inprogress = :expr and currentIncident = :ci")
                .withValueMap(new ValueMap().with(":e", new Date().toString()).withNumber(":c", 0).withBoolean(":i", false).withNumber(":ed", System.currentTimeMillis())
                        .withBoolean(":expr", false).withNumber(":ci", 1))
                .withReturnValues(ReturnValue.UPDATED_NEW);

        try {
            System.out.println("Updating the item...");
            UpdateItemOutcome outcome = table.updateItem(updateItemSpec);
            System.out.println("UpdateItem succeeded:\n" + outcome.getItem().toJSONPretty());

        }
        catch (Exception e) {
            System.err.println("Unable to update item: " );
            System.out.println(e.getMessage());
            return false;
        }
        return true;
    }

    public static boolean updateDuplicate(Table table, String event_id, String detector_id) {

        UpdateItemSpec updateItemSpec = new UpdateItemSpec().withPrimaryKey("event_id", event_id, "detector_Id", detector_id)
                .withUpdateExpression("set duplicate= :d")
                .withConditionExpression("detector_Id = :di and currentIncident = :ci")
                .withValueMap(new ValueMap().withBoolean(":d", true)
                        .withNumber(":ci", 1).withString(":di", detector_id))
                .withReturnValues(ReturnValue.UPDATED_NEW);

        try {
            System.out.println("Updating the item...");
            UpdateItemOutcome outcome = table.updateItem(updateItemSpec);
            System.out.println("UpdateItem succeeded:\n" + outcome.getItem().toJSONPretty());

        }
        catch (Exception e) {
            System.err.println("Unable to update item: " );
            return false;
        }
        return true;
    }

    public static ItemCollection<ScanOutcome> getCurrentIncidents(Table table) {
        ScanSpec spec = new ScanSpec()
                .withFilterExpression("currentIncident = :v_current_incident")
                .withValueMap(new ValueMap()
                        .withNumber(":v_current_incident", 1))
                .withConsistentRead(true);

        ItemCollection<ScanOutcome> items = null;// table.scan(spec);
        try {
            System.out.println("Attempting to read the item...");
            items = table.scan(spec);
            System.out.println("GetItem succeeded: " + items);
        }
        catch (Exception e) {
            System.err.println("Unable to read items ");
            System.err.println(e.getMessage());
        }
        return items;
    }

    public static ItemCollection<ScanOutcome> getOldIncidents(Table table, long startTime) {
        System.out.println(startTime);
        ScanSpec spec = new ScanSpec()
                .withFilterExpression("currentIncident = :v_current_incident and end_timestamp_date_format >= :v_time")
                .withValueMap(new ValueMap()
                        .withNumber(":v_current_incident", 1)
                .withNumber(":v_time", startTime))
                .withConsistentRead(true);

        ItemCollection<ScanOutcome> items = null;// table.scan(spec);
        try {
            System.out.println("Attempting to read the item...");
            items = table.scan(spec);
            System.out.println("GetItem succeeded: " + items);
            Iterator<Item> iter = items.iterator();
            while (iter.hasNext()) {
                Item item = iter.next();
                System.out.println(item.toString());
            }
        }
        catch (Exception e) {
            System.err.println("Unable to read items ");
            System.err.println(e.getMessage());
        }
        //1554137410139
        //1554052946027
        return items;
    }

    public static void main(String[] args)  {
        AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard()
//                .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration("http://localhost:8000", "us-west-2"))
                .withRegion(Regions.US_EAST_1)
                .build();

        DynamoDB dynamoDB = new DynamoDB(client);
        Table table = dynamoDB.getTable("iawavetronixincident");

        Date previousDate = DateUtils.getPreviousDate(new Date(), 1);
        boolean b = updateItem(table, "01-04-2019_1554155684870_ADR8110 - IA 192 SB @ MM 3.75", "ADR8110 - IA 192 SB @ MM 3.75");
        System.out.println(b);
//        getOldIncidents(table, previousDate.getTime());
    }

}

