package com.etalyc.mcomp.incidentdetection.streams;

import com.etalyc.mcomp.incidentdetection.misc.ExternalConfig;
import com.etalyc.mcomp.incidentdetection.utils.JavaCustomInrixReceiver;
import org.apache.log4j.Logger;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import redis.clients.jedis.Jedis;

import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.Properties;

public class ReadingStreams {

    private static Jedis jedis;

    private static Logger log;

    private static JavaStreamingContext jssc;


    public static void main(String[] args) throws Exception {

        ExternalConfig configProperties = getConfigProperties();

        initialize(configProperties);

        executeInrixStreams(configProperties);

        startStreamingContext();

    }

    private static void startStreamingContext() throws InterruptedException {
        // Start the computation*/
        //================================================================================================================
        jssc.start();
        jssc.awaitTermination();
    }

    private static void executeInrixStreams(ExternalConfig configProperties) throws Exception {
        System.out.println(configProperties.toString());
        JavaDStream<String> inrixData = jssc.receiverStream(new JavaCustomInrixReceiver(configProperties.getInrixDirPath(),
                configProperties.getInrixFilename()));
        InrixStreams.initialize(configProperties);
        InrixStreams.processInrixStreams(inrixData, configProperties);

    }

    private static void initialize(ExternalConfig config) throws Exception {

        jedis = new Jedis("localhost");
        log = SparkStreamConfiguration.setLogLevels();
        JavaSparkContext jsc = SparkStreamConfiguration.createJavaSparkContext(config);
        jsc.setLogLevel("ERROR");

        jssc = new JavaStreamingContext(jsc, Durations.seconds(20));
    }

    private static ExternalConfig getConfigProperties() throws Exception {
        Properties mainProperties = new Properties();
            mainProperties.load(new FileInputStream("./application.properties"));
        ExternalConfig config = new ExternalConfig();
        config.setInrixDirPath(mainProperties.getProperty("archive.wavetronix.realtimedata.folder"));
//        config.setMongoDbName(mainProperties.getProperty("mongoDbName"));
//        config.setMongoCollectionNameForProcessedInrixData(mainProperties.getProperty("mongoCollectionNameForProcessedWavetronixData"));
//        config.setMongoCollectionNameForRawInrixData(mainProperties.getProperty("mongoCollectionNameForRawWavetronixData"));
//        config.setMongoConnectionString(mainProperties.getProperty("mongoConnectionString"));
        config.setIncidentsOccurredInPastInMinutes(mainProperties.getProperty("incidentsOccurredInPastInMinutes"));
        config.setDynamoDbTableName(mainProperties.getProperty("dynamoDbTableName"));
        String today = new SimpleDateFormat("yyyyMMdd").format(new java.util.Date());
        System.out.println(today + ".csv");
        config.setInrixFilename(today + ".csv");
        return config;
    }
}
