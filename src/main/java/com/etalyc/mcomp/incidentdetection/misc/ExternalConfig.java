package com.etalyc.mcomp.incidentdetection.misc;

public class ExternalConfig {

    private String inrixDirPath;

    public String getDynamoDbTableName() {
        return dynamoDbTableName;
    }

    public void setDynamoDbTableName(String dynamoDbTableName) {
        this.dynamoDbTableName = dynamoDbTableName;
    }

    private String dynamoDbTableName;

    public String getInrixFilename() {
        return inrixFilename;
    }

    public void setInrixFilename(String inrixFilename) {
        this.inrixFilename = inrixFilename;
    }

    private String inrixFilename;
    private String incidentsOccurredInPastInMinutes;
//    private String

    public ExternalConfig() {
    }

    public ExternalConfig(String inrixDirPath, String inrixFilename, String incidentsOccurredInPastInMinutes, String dynamoDbTableName) {
        this.inrixDirPath = inrixDirPath;
        this.incidentsOccurredInPastInMinutes = incidentsOccurredInPastInMinutes;
        this.inrixFilename = inrixFilename;
        this.dynamoDbTableName = dynamoDbTableName;
    }


    public String getInrixDirPath() {
        return inrixDirPath;
    }

    public void setInrixDirPath(String inrixDirPath) {
        this.inrixDirPath = inrixDirPath;
    }



    public String getIncidentsOccurredInPastInMinutes() {
        return incidentsOccurredInPastInMinutes;
    }

    public void setIncidentsOccurredInPastInMinutes(String incidentsOccurredInPastInMinutes) {
        this.incidentsOccurredInPastInMinutes = incidentsOccurredInPastInMinutes;
    }

    @Override
    public String toString() {
        return "ExternalConfig{" +
                "inrixDirPath='" + inrixDirPath + '\'' +
                ", dynamoDbTableName='" + dynamoDbTableName + '\'' +
                ", inrixFilename='" + inrixFilename + '\'' +
                ", incidentsOccurredInPastInMinutes='" + incidentsOccurredInPastInMinutes + '\'' +
                '}';
    }
}
